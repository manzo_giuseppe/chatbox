package org.manzocad.chatbox;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

@Controller
@RequestMapping("chat")
public class ChatController {

    private ArrayList<SseEmitter> emitters = new ArrayList<SseEmitter>();
    private Logger logger = Logger.getLogger(getClass().getName());

    @GetMapping("register/{nick}")
    public SseEmitter register(@PathVariable("nick") String nick){
        SseEmitter emitter = new SseEmitter(1000000l);

        emitter.onCompletion(()->{
            logger.info(nick+" COMPLETED");
            synchronized (this) {
                emitters.remove(emitter);
            }
        });

        emitter.onTimeout(()->{
            logger.info(nick+" TIMED OUT");
            synchronized (this) {
                emitters.remove(emitter);
            }
        });

        synchronized (this) {
            emitters.add(emitter);
        }

        send(nick+ " entered in chat!");

        return emitter;
    }

    @PostMapping("send")
    public ResponseEntity send(@RequestBody String message){

        Executor ex = Executors.newSingleThreadExecutor();

        ex.execute(()->{
            synchronized (this) {
                emitters.forEach((emitter) -> {
                    try {
                        emitter.send(message, MediaType.TEXT_PLAIN);
                    } catch (IOException e) {
                        e.printStackTrace();
                        emitter.complete();
                        emitters.remove(emitter);
                    }
                });
            }
        });

        return new ResponseEntity(HttpStatus.OK);
    }

}
